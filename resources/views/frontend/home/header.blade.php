<nav class="navbar navbar-light bg-light static-top">
    <div class="container">
        <a href="{{ url('/') }}"> <img src="{{ asset('assets/images/nexco-logo1.png') }}" height="50" width="130"/></a>
{{--        <a class="btn btn-primary" href="#" id="button">Sign In</a>--}}
        @guest
        <a class="btn btn-primary" href="{{ route('login') }}" id="button1">{{ __('Sign In') }}</a>
        @else
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </ul>
                </li>
            </ul>
        @endguest
    </div>
</nav>
<!-- Masthead -->
<header class="masthead text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <h1 class="mb-5">Your path to success starts with Nexco.</h1>
            </div>
            <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
                <form>
                    <div class="input-group md-form form-sm form-2 pl-0">
                        <input class="form-control my-0 py-1 lime-border" type="text"
                               placeholder="Search Interested Course" aria-label="Search">
                        <div class="input-group-append">
                        <span class="input-group-text lime lighten-2" id="basic-text1">
                            <i class="fas fa-search text-blue" aria-hidden="true"></i>
                        </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</header>
<div class="popup" style="display: none;">
    <div class="popup-content">
        <img src="{{ asset('assets/images/close.png') }}" alt="close" class="close">
        <img src="{{ asset('assets/images/nexco-logo1.png') }}" class="logo">
        <input type="text" placeholder="Username" name="username">
        <input type="password" placeholder="Password" name="password">
        <a href="#" class="btn btn-primary"> Sign In </a>
    </div>
</div>
