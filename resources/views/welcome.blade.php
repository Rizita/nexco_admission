@extends('layout')
@section('title'){{ __('Nexcoo') }} @endsection
@section('content')
    <!-- Icons Grid -->
    <section class="features-icons bg-light text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                        <div class="features-icons-icon d-flex">
                            <i class="fas fa-user-graduate m-auto text-primary"></i>

                        </div>
                        <div class="edu">
                            <a href="education.html"><h3>Education</h3></a>
                            <p class="lead mb-0">University and Colleges of your Choice!</p></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                        <div class="features-icons-icon d-flex">
                            <i class="far fa-newspaper m-auto text-primary"></i>

                        </div>
                        <div class="edu">
                            <a href="services.html"><h3>Nexco Services</h3></a>
                            <p class="lead mb-0">Everything about Nexco from the Establishment to Services Provided!</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                        <div class="features-icons-icon d-flex">
                            <i class="fas fa-paper-plane m-auto text-primary"></i>

                        </div>
                        <div class="edu">
                            <a href="contact"><h3>Communication</h3></a>
                            <p class="lead mb-0">Contact Nexco Team by filling up your Details!</p></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--Carousel Wrapper-->
    <div id="multi-item-example" class="carousel slide carousel-multi-item carousel-multi-item-2" data-ride="carousel">

        <div class="text-center">
            <br>
            <h1> Nexco's Partner Provider</h1>
            <br>
        </div>

        <!--Slides-->
        <div class="carousel-inner" role="listbox">

            <!--First slide-->
            <div class="carousel-item active">

                <div class="col-md-3 mb-3">
                    <div class="card">
                        <img class="img-fluid" src="img/1.jpg"
                             alt="Card image cap">
                        <img class="img-fluid" src="{{ asset('assets/images/1.jpg') }}" alt="Card image cap">
                    </div>
                </div>

                <div class="col-md-3 mb-3">
                    <div class="card">
                        <img class="img-fluid" src="{{ asset('assets/images/2.png') }}" alt="Card image cap">
                    </div>
                </div>

                <div class="col-md-3 mb-3">
                    <div class="card">
                        <img class="img-fluid" src="{{ asset('assets/images/3.jpg') }}" alt="Card image cap">
                    </div>
                </div>

                <div class="col-md-3 mb-3">
                    <div class="card">
                        <img class="img-fluid" src="{{ asset('assets/images/4.png') }}" alt="Card image cap">
                    </div>
                </div>

            </div>
            <!--/.First slide-->

            <!--Second slide-->
            <div class="carousel-item">

                <div class="col-md-3 mb-3">
                    <div class="card">
                        <img class="img-fluid" src="{{ asset('assets/images/5.png') }}" alt="Card image cap">
                    </div>
                </div>

                <div class="col-md-3 mb-3">
                    <div class="card">
                        <img class="img-fluid" src="{{ asset('assets/images/6.jpg') }}" alt="Card image cap">
                    </div>
                </div>

                <div class="col-md-3 mb-3">
                    <div class="card">
                        <img class="img-fluid" src="{{ asset('assets/images/7.jpg') }}" alt="Card image cap">
                    </div>
                </div>

                <div class="col-md-3 mb-3">
                    <div class="card">
                        <img class="img-fluid" src="{{ asset('assets/images/8.png') }}" alt="Card image cap">
                    </div>
                </div>

            </div>
            <!--/.Second slide-->

            <!--Third slide-->
            <div class="carousel-item">

                <div class="col-md-3 mb-3">
                    <div class="card">
                        <img class="img-fluid" src="{{ asset('assets/images/9.png') }}" alt="Card image cap">
                    </div>
                </div>

                <div class="col-md-3 mb-3">
                    <div class="card">
                        <img class="img-fluid" src="{{ asset('assets/images/10.jpg') }}" alt="Card image cap">
                    </div>
                </div>

                <div class="col-md-3 mb-3">
                    <div class="card">
                        <img class="img-fluid" src="{{ asset('assets/images/11.jpg') }}" alt="Card image cap">
                    </div>
                </div>

                <div class="col-md-3 mb-3">
                    <div class="card">
                        <img class="img-fluid" src="{{ asset('assets/images/12.png') }}" alt="Card image cap">
                    </div>
                </div>

            </div>
            <!--/.Third slide-->

        </div>
        <!--/.Slides-->
        <!--Controls-->
        <div class="text-center">
            <a class="black-text" href="#multi-item-example" data-slide="prev"><i
                    class="fas fa-angle-left fa-3x pr-3"></i></a>
            <a class="black-text" href="#multi-item-example" data-slide="next"><i
                    class="fas fa-angle-right fa-3x pl-3"></i></a>
        </div>
        <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->
    <!-- Testimonials -->
    <section class="testimonials text-center bg-light">
        <div class="container">
            <h1 class="mb-5">Students' Review on Nexco...</h1>
            <div class="row">
                <div class="col-lg-4">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                        <img class="img-fluid rounded-circle mb-3" src="{{ asset('assets/images/testimonials-1.jpg') }}" alt="">

                        <h5>Margaret E.</h5>
                        <p class="font-weight-light mb-0">"Nexco is fantastic! Thanks so much guys!"</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                        <img class="img-fluid rounded-circle mb-3" src="{{ asset('assets/images/testimonials-2.jpg') }}" alt="">
                        <h5>Fred S.</h5>
                        <p class="font-weight-light mb-0">"All credit goes to Nexco for my successful career!"</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                        <img class="img-fluid rounded-circle mb-3" src="{{ asset('assets/images/testimonials-3.jpg') }}" alt="">
                        <h5>Sarah W.</h5>
                        <p class="font-weight-light mb-0">"Thanks so much for making my future bright!"</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Call to Action -->
    <section class="call-to-action text-white text-center">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <h2 class="mb-4">Ready to Study in Abroad? Register now!</h2>
                </div>
                <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
                    <form>
                        <div class="form-row">
                            <div class="col-12 col-md-9 mb-2 mb-md-0">
                                <input type="email" class="form-control form-control-lg"
                                       placeholder="Enter your email...">

                            </div>
                            <div class="col-12 col-md-3">
                                <button type="submit" class="btn btn-block btn-lg btn-primary">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
