<label for="{{ $field['name'] }}" class="custom-checkbox mb-3 checkbox-primary d-block">
    <input type="hidden" name="{{ $field['name'] }}" value="0">
    <input id="{{ $field['name'] }}" type="checkbox"
           name="{{ $field['name'] }}" {{old($field['name'], \setting($field['name'])) ? 'checked' : '' }} value="1">
    <span class="label-helper">{{ $field['label'] }}</span>
</label>
