<div class="form-group {{ $errors->has($field['name'])?' is-invalid':'' }}">
    {!! Html::decode(Form::label($field['name'], $field['label'], ['class' => 'control-label']))  !!}
    <div class="input-group">
           <span class="input-group-btn">
             <a data-input="{{ $field['name'].'_input_url' }}" data-preview="{{ $field['name'].'_input_holder' }}"  class="js-media-manager-button btn btn-light btn-icon">
               <i class="fa fa-image"></i> Choose
             </a>
           </span>
        {{ Form::text($field['name'],old($field['name'], \setting($field['name'])),['class'=>'form-control', 'id'=>$field['name'].'_input_url']) }}
    </div>
    <div class="preview-wrapper position-relative">
        <img id="{{ $field['name'].'_input_holder' }}" style="margin-top:15px;max-height:100px;" src="{{ old($field['name'], \setting($field['name'])) }}">
        <a href="javascript:void(0);" class="remove-featured-image position-absolute text-dark-active" style="left:2px; top:15px; display:{{\setting($field['name'])?'block':'none'}}"><i class="fa fa-times-circle-o"></i></a>
    </div>
    @if ($errors->has($field['name']))
        <div class="invalid-feedback animated fadeInDown">
            {{ $errors->first($field['name']) }}
        </div>
    @endif
</div>
