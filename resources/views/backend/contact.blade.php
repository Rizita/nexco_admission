<!DOCTYPE html>
<html lang="en">
<head>
  <title>Nexco</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Student's Enquiry</h2>
       
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>View Details</th>
      </tr>
    </thead>
    <tbody>
    @foreach($contacts as $contact)
      <tr>
        <td>{{$contact->first_name}}</td>
        <td>{{$contact->email}}</td>
        <td><a href="/show/{{$contact->id}}" class="btn btn-primary">View</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

</body>
</html>
