<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>@yield('title')</title>
    {{--  <title>@yield('title') - {{ setting('site_name',config('app.name')) }}</title>--}}
    <link href="{{mix('css/app.css')}}" rel="stylesheet" />
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">{{-- inside css of public dir--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
</head>
<body>
<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
    @include('backend.header')

</nav>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        @include('backend.sidebar')
    </div>
    <div id="layoutSidenav_content">
        @yield('content')
    </div>

        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Nexco 2020</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ asset('admin/js/scripts.js') }}"></script>
</body>
</html>
