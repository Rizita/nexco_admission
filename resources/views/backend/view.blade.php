<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
 
<div class="container">
  <h2>Student's Enquiry</h2>
  <div class="panel-group">
  
    <div class="panel panel-default">
      <div class="panel-heading"><h4>{{$contacts->first_name}} <?php "\n"?> {{$contacts->last_name}} </h4></div>
      <div class="panel-body">
          <table>
              <tr>
                  <td><b>Email:</b>{{$contacts->email}}</td>
                </tr>
                <tr>
                  <td><b>Phone Number:</b>{{$contacts->phone}}</td>
                </tr>
                <tr>
                  <td><b>Message:</b>{{$contacts->message}}</td>
                </tr>
                <tr>
                  <td><br><a href="/reply/{{$contacts->id}}" class="btn btn-info">Reply</a></td>
                </tr>
            </table>
                
        </div>
        </div>
    
 </div>
</div>

</body>
</html>
