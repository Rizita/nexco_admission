<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Setting;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SettingsController extends Controller
{
    protected $settings,$page;
    public function __construct(Setting $setting, Page $page)
    {
        $this->setting = $setting;
        $this->page = $page;
    }

    public function index()
    {
        $pages = $this->page->all()->pluck('title','id')->toArray();
        return view('backend.settings.index', compact('pages'));
    }

    public function store(Request $request)
    {
        $rules = $this->setting->getValidationRules();
        $data = $this->validate($request, $rules);
        $validSettings = array_keys($rules);
        $tab_hash = $request->input('active_tab')?$request->input('active_tab'):null;
        foreach ($data as $key => $val) {
            if(in_array($key, $validSettings)) {
                $this->setting->add($key, $val, $this->setting->getDataType($key));
            }
        }
        return redirect()->route('settings.index')->with('success','Setting Successfully Saved')->with('tab_hash', $tab_hash);
    }
}
