<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;

class ContactController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $contacts = \App\Contact::all();

        return view('backend.contact',compact('contacts'));
    }

    public function show($id){
        $contacts = \App\Contact::findorFail($id);

        return view('backend.view',compact('contacts'));
        // $contact = Contact::find($id);
        // return view('view')->with('contact','$contact');
    }

    public function reply($id){
        $contacts = \App\Contact::findorFail($id);

        return view('backend.reply',compact('contacts'));
    }

    public function save(Request $request)
    {
        $validatedData = $request->validate([
            'fname' => 'required',
            'lname' => 'required',
            'email'=>'required|email',
            'phone'=>'required',
            'message'=>'required',
        ]);
        $contact = new Contact([
            'first_name'=> $request->get('fname'),
            'last_name'=> $request->get('lname'),
            'email'=> $request->get('email'),
            'phone'=> $request->get('phone'),
            'message'=> $request->get('message'),
        ]);
        $contact->save();
        return redirect('/contact')->with('success','Your Enquiry has been successfully sent');
    }
}
