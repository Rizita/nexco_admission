<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public $timestamps = false;
    protected $fillable = ['user_id','full_name', 'nickname', 'display_name', 'avatar', 'description', 'website'];
    protected $hidden =['user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
