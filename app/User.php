<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

use Plank\Metable\Metable;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use Metable;

    const ACTIVE = 1;
    const BANNED = 0;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'active', 'password', 'remember_token', 'email_verified_at'
    ];
    protected $appends = ['fullname'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
//    protected $casts = [
//        'email_verified_at' => 'datetime',
//    ];

    public function profile(){
        return $this->hasOne(Profile::class);
    }

    public function getFullnameAttribute()
    {
        if($this->profile())
        {
            return ucfirst($this->profile->display_name?$this->profile->display_name:$this->profile->full_name);
        }
        return ucfirst($this->username);
    }
}
