<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('course_code');
            $table->string('name');
            $table->string('year');
            $table->integer('semester');
            $table->string('description');
            $table->text('total_credit');
            $table->text('annual_fee');
            $table->text('degree')->nullable(false)->change();//level of study
            $table->unsignedInteger('university_id');
            $table->unsignedInteger('category_id');
            $table->foreign('university_id')->references('id')->on('university')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('category')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
